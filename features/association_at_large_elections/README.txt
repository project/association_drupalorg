General instructions

Modules required:
Reply (7.x-1.x-dev version, 01/08/2013)
Election (7.x-1.0-beta19)
Views Bulk Operations (dependency of election) (7.x-3.1)
Twitterfield (7.x-1.0-rc1)

Run drush updatedb to install and set up all dependencies.

Run drush fr association_at_large_elections to create all required fields and elements.

Access elections path on the ADO browser http://association.drupal.org/elections and click on "Add new STV election"

Add a new position. Be sure to set the following options:
 - The option "Do not include RON" should be selected.
 - Roles eligible to vote: authenticated users
 - Under Candidates and nominations, the option "Exclusive" should be ticked.
 - All the statuses should be set to "Inherited"
 - Allow abstention and equal rankings should be disabled.
 - The option "User is eligible to vote" under the Conditional voting options should be enabled.

Set variable "association_at_large_elections_block_nomination" to the id of the position just created.

The table name for loading the electorate is "association_at_large_elections_electorate"
