<?php

/**
 * @file
 * ado_permissions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function ado_permissions_user_default_roles() {
  $roles = array();

  // Exported role: Staff Access Confidential Data.
  $roles['Staff Access Confidential Data'] = array(
    'name' => 'Staff Access Confidential Data',
    'weight' => 12,
  );

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 0,
  );

  // Exported role: association.
  $roles['association'] = array(
    'name' => 'association',
    'weight' => 6,
  );

  // Exported role: community.
  $roles['community'] = array(
    'name' => 'community',
    'weight' => 5,
  );

  // Exported role: confirmed.
  $roles['confirmed'] = array(
    'name' => 'confirmed',
    'weight' => 4,
  );

  // Exported role: elections committee.
  $roles['elections committee'] = array(
    'name' => 'elections committee',
    'weight' => 11,
  );

  return $roles;
}
