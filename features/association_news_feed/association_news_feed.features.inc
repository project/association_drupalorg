<?php
/**
 * @file
 * association_news_feed.features.inc
 */

/**
 * Implements hook_views_api().
 */
function association_news_feed_views_api() {
  return array("version" => "3.0");
}
