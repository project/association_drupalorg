<?php

/**
 * @file
 * Drush commands for Association site.
 */

/**
 * Implementation of hook_drush_command().
 */
function association_drupalorg_drush_command() {
  return array(
    'user-data-clean' => array(
      'callback' => 'association_drupalorg_drush_user_data_cleaner',
      'description' => 'Clean out the users.data column of profile fields.',
    ),
    'association-drupalorg-migrate-blogs' => array(
      'callback'=> 'association_drupalorg_migrate_blogs',
      'description' => 'Export the blog entries of profile fields.',
    ),
  );
}

/**
 * Implementation of hook_drush_help().
 */
function association_drupalorg_drush_help($section) {
  switch ($section) {
    case 'drush:user-data-clean':
      return dt('Cleans out the users.data field. This loops over all users, loads the row, modifies, updates it back. Could take a while.');
  }
}

/**
 * Callback for drush command.
 * Removes profile fields from users.data
 */
function association_drupalorg_drush_user_data_cleaner() {
  $max = db_result(db_query("SELECT max(uid) from {users}"));
  for ($i = 1; $i < ($max + 3); $i++) {
    $account = db_fetch_object(db_query("SELECT * FROM {users} WHERE uid = %d", $i));
    if ($account && $account->status == 1) {
      $data = unserialize($account->data);
      unset($data['profile_current_company_organization']);
      unset($data['profile_full_name']);
      unset($data['profile_job']);
      unset($data['profile_role_with_drupal']);
      unset($data['country']);
      unset($data['profile_irc_nick']);
      db_query("UPDATE {users} SET data = '%s' WHERE uid = %d", serialize($data), $i);
    }
  }
}

function association_drupalorg_migrate_blogs() {
  $blog_query = new EntityFieldQuery();
  $result = $blog_query->entityCondition('entity_type', 'node')
    ->propertyCondition('promote', 1)
    ->execute();

  $blog_post_nids = array_keys($result['node']);

  // Combine the array with itself to make keys equal to values
  $blog_post_nids = array_combine($blog_post_nids, $blog_post_nids);

  $comment_query = new EntityFieldQuery();
  $comment_result = $comment_query->entityCondition('entity_type', 'comment')
    ->propertyCondition('nid', [$blog_post_nids])
    ->execute();
  $comment_nodes = comment_load_multiple(array_keys($comment_result['comment']));

  $blog_data = [];
  foreach (node_load_multiple(array_keys($result['node'])) as $nid => $blog) {
    if ($blog->status == 1) {
      $blog_data[$nid]['title'] = $blog->title;
      $blog_data[$nid]['name'] = $blog->name;
      $blog_data[$nid]['created'] = $blog->created;
      $blog_data[$nid]['changed'] = $blog->changed;
      $blog_data[$nid]['body'] = $blog->body[LANGUAGE_NONE][0]['value'];
      if(!empty($blog->upload)) {
        $blog_data[$nid]['files'] = $blog->upload;
      }

      $blog_data[$nid]['comment_count'] = $blog->comment_count > 0 ? $blog->comment_count : 0;
    }
  }

  // If a blog post has comments, attach them.
  foreach ($comment_nodes as $cid => $comment) {
    if (($comment->uid != 0) && array_key_exists($comment->nid, $blog_data)) {
      $blog_data[$comment->nid]['comments'][$cid] = $comment;
      if ($blog_data[$comment->nid]['comments'][$cid]->uid != 0)  {
        $assoc_account = user_load($blog_data[$comment->nid]['comments'][$cid]->uid);
        $drupalorg_account_uid = drupalorg_crosssite_get_drupalorg_uid($assoc_account);
        echo 'cid: '. $cid . ' ado uid: ' . $assoc_account->uid . ' uid: ' . $drupalorg_account_uid . PHP_EOL;
        $blog_data[$comment->nid]['comments'][$cid]->uid = $drupalorg_account_uid;
      }
    }
  }

  // Format the JSON data with pretty printing
  $formatted_json = json_encode($blog_data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

  $formatted_image_list_json = json_encode($public_urls, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

  // Save the JSON data to a file
  $file_path = 'blogs_export.json';
  file_put_contents($file_path, $formatted_json);

  echo "Blog data exported successfully to $file_path\n";
}
