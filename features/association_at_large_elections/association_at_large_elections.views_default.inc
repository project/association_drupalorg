<?php

/**
 * @file
 * association_at_large_elections.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function association_at_large_elections_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'nominations';
  $view->description = 'A list of candidates (intended for the public / all viewers of the election).';
  $view->tag = 'election';
  $view->base_table = 'election_candidate';
  $view->human_name = 'Nominations';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Candidates for the Position of: Director at Large';
  $handler->display->display_options['css_class'] = 'election-candidate';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'time';
  $handler->display->display_options['cache']['results_lifespan'] = '-1';
  $handler->display->display_options['cache']['results_lifespan_custom'] = '0';
  $handler->display->display_options['cache']['output_lifespan'] = '-1';
  $handler->display->display_options['cache']['output_lifespan_custom'] = '0';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['slave'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '0';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['row_class'] = 'elec-list';
  $handler->display->display_options['style_options']['class'] = 'election-candidate';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = 'Here are the candidates for the 2014 Drupal Association election for Directors "At Large". Two directors will be elected. Please use the links below to view each candidate\'s profile. You may ask questions of individual candidates by posting comments on their  profiles. The "replies" column shows how much discussion is taking place with each candidate. Chat logs, audio recordings and questions from "Meet the Candidate" sessions can be found on groups.drupal.org. ';
  $handler->display->display_options['header']['area']['format'] = 'filtered_html';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area_1']['id'] = 'area_1';
  $handler->display->display_options['header']['area_1']['table'] = 'views';
  $handler->display->display_options['header']['area_1']['field'] = 'area';
  $handler->display->display_options['header']['area_1']['content'] = '<h2>Etiquette</h2>
<ul><li>Please don\'t comment only to endorse a particular candidate. Endorsements clutter up discussion. Instead, pose questions that you and others can learn from.</li>
<li>Please avoid negative comments about candidates. Any comments containing personal criticism may be unpublished.</li>';
  $handler->display->display_options['header']['area_1']['format'] = 'full_html';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'There are currently no published candidate profiles for this election';
  $handler->display->display_options['empty']['area']['content'] = 'There are currently no published candidate profiles for this election.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Relationship: Election candidate: User */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'election_candidate';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  $handler->display->display_options['relationships']['uid']['required'] = TRUE;
  /* Field: Election candidate: Name */
  $handler->display->display_options['fields']['candidate_name']['id'] = 'candidate_name';
  $handler->display->display_options['fields']['candidate_name']['table'] = 'field_data_candidate_name';
  $handler->display->display_options['fields']['candidate_name']['field'] = 'candidate_name';
  $handler->display->display_options['fields']['candidate_name']['label'] = '';
  $handler->display->display_options['fields']['candidate_name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['candidate_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['candidate_name']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['candidate_name']['click_sort_column'] = 'title';
  $handler->display->display_options['fields']['candidate_name']['settings'] = array(
    'format' => 'default',
    'markup' => 0,
    'output' => 'default',
    'multiple' => 'default',
    'multiple_delimiter' => ', ',
    'multiple_and' => 'text',
    'multiple_delimiter_precedes_last' => 'never',
    'multiple_el_al_min' => '3',
    'multiple_el_al_first' => '1',
  );
  /* Field: Election candidate: Picture */
  $handler->display->display_options['fields']['field_election_picture']['id'] = 'field_election_picture';
  $handler->display->display_options['fields']['field_election_picture']['table'] = 'field_data_field_election_picture';
  $handler->display->display_options['fields']['field_election_picture']['field'] = 'field_election_picture';
  $handler->display->display_options['fields']['field_election_picture']['label'] = '';
  $handler->display->display_options['fields']['field_election_picture']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_election_picture']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_election_picture']['settings'] = array(
    'image_style' => 'candidate_thumbnail',
    'image_link' => 'content',
  );
  /* Field: User: Name */
  $handler->display->display_options['fields']['name_1']['id'] = 'name_1';
  $handler->display->display_options['fields']['name_1']['table'] = 'users';
  $handler->display->display_options['fields']['name_1']['field'] = 'name';
  $handler->display->display_options['fields']['name_1']['relationship'] = 'uid';
  $handler->display->display_options['fields']['name_1']['label'] = '';
  $handler->display->display_options['fields']['name_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['name_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name_1']['link_to_user'] = FALSE;
  /* Field: User: Drupal.org uid */
  $handler->display->display_options['fields']['drupalorg_uid']['id'] = 'drupalorg_uid';
  $handler->display->display_options['fields']['drupalorg_uid']['table'] = 'views_entity_user';
  $handler->display->display_options['fields']['drupalorg_uid']['field'] = 'drupalorg_uid';
  $handler->display->display_options['fields']['drupalorg_uid']['relationship'] = 'uid';
  $handler->display->display_options['fields']['drupalorg_uid']['label'] = '';
  $handler->display->display_options['fields']['drupalorg_uid']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['drupalorg_uid']['alter']['text'] = '[name_1]';
  $handler->display->display_options['fields']['drupalorg_uid']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['drupalorg_uid']['alter']['path'] = 'https://www.drupal.org/user/[drupalorg_uid]';
  $handler->display->display_options['fields']['drupalorg_uid']['alter']['absolute'] = TRUE;
  $handler->display->display_options['fields']['drupalorg_uid']['element_class'] = 'druprofilelink';
  $handler->display->display_options['fields']['drupalorg_uid']['element_label_colon'] = FALSE;
  /* Field: Field: Crosssite account created */
  $handler->display->display_options['fields']['field_crosssite_account_created']['id'] = 'field_crosssite_account_created';
  $handler->display->display_options['fields']['field_crosssite_account_created']['table'] = 'field_data_field_crosssite_account_created';
  $handler->display->display_options['fields']['field_crosssite_account_created']['field'] = 'field_crosssite_account_created';
  $handler->display->display_options['fields']['field_crosssite_account_created']['relationship'] = 'uid';
  $handler->display->display_options['fields']['field_crosssite_account_created']['label'] = 'Drupal.org Account Age';
  $handler->display->display_options['fields']['field_crosssite_account_created']['type'] = 'format_interval';
  $handler->display->display_options['fields']['field_crosssite_account_created']['settings'] = array(
    'interval' => '2',
    'interval_display' => 'raw time ago',
  );
  /* Field: Election candidate: Twitter handle */
  $handler->display->display_options['fields']['field_twitter']['id'] = 'field_twitter';
  $handler->display->display_options['fields']['field_twitter']['table'] = 'field_data_field_twitter';
  $handler->display->display_options['fields']['field_twitter']['field'] = 'field_twitter';
  $handler->display->display_options['fields']['field_twitter']['label'] = '';
  $handler->display->display_options['fields']['field_twitter']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_twitter']['type'] = 'twitterfield_twitter_link';
  /* Field: Election candidate: Statement of Candidacy Video */
  $handler->display->display_options['fields']['field_youtube_video_link']['id'] = 'field_youtube_video_link';
  $handler->display->display_options['fields']['field_youtube_video_link']['table'] = 'field_data_field_youtube_video_link';
  $handler->display->display_options['fields']['field_youtube_video_link']['field'] = 'field_youtube_video_link';
  $handler->display->display_options['fields']['field_youtube_video_link']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_youtube_video_link']['alter']['text'] = '<a href="[field_youtube_video_link-url]">[name_1]\'s statement of candidacy</a>';
  $handler->display->display_options['fields']['field_youtube_video_link']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_youtube_video_link']['alter']['path'] = '[field_youtube_video_link-url]';
  $handler->display->display_options['fields']['field_youtube_video_link']['click_sort_column'] = 'url';
  /* Field: Election candidate: Candidate ID */
  $handler->display->display_options['fields']['candidate_id']['id'] = 'candidate_id';
  $handler->display->display_options['fields']['candidate_id']['table'] = 'election_candidate';
  $handler->display->display_options['fields']['candidate_id']['field'] = 'candidate_id';
  $handler->display->display_options['fields']['candidate_id']['label'] = '';
  $handler->display->display_options['fields']['candidate_id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['candidate_id']['element_label_colon'] = FALSE;
  /* Field: Election candidate: Profile link */
  $handler->display->display_options['fields']['view_link']['id'] = 'view_link';
  $handler->display->display_options['fields']['view_link']['table'] = 'election_candidate';
  $handler->display->display_options['fields']['view_link']['field'] = 'view_link';
  $handler->display->display_options['fields']['view_link']['label'] = '';
  $handler->display->display_options['fields']['view_link']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['view_link']['alter']['text'] = 'Candidate Profile';
  $handler->display->display_options['fields']['view_link']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['view_link']['alter']['link_class'] = 'action-button';
  $handler->display->display_options['fields']['view_link']['element_label_colon'] = FALSE;
  /* Sort criterion: Global: Random */
  $handler->display->display_options['sorts']['random']['id'] = 'random';
  $handler->display->display_options['sorts']['random']['table'] = 'views';
  $handler->display->display_options['sorts']['random']['field'] = 'random';
  /* Contextual filter: Election: Election ID */
  $handler->display->display_options['arguments']['election_id']['id'] = 'election_id';
  $handler->display->display_options['arguments']['election_id']['table'] = 'election';
  $handler->display->display_options['arguments']['election_id']['field'] = 'election_id';
  $handler->display->display_options['arguments']['election_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['election_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['election_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['election_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['election_id']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['election_id']['validate']['type'] = 'numeric';
  $handler->display->display_options['arguments']['election_id']['validate']['fail'] = 'empty';
  /* Filter criterion: Election: Published or admin */
  $handler->display->display_options['filters']['published_or_admin']['id'] = 'published_or_admin';
  $handler->display->display_options['filters']['published_or_admin']['table'] = 'election';
  $handler->display->display_options['filters']['published_or_admin']['field'] = 'published_or_admin';
  $handler->display->display_options['filters']['published_or_admin']['group'] = 1;
  /* Filter criterion: Election candidate: Published status */
  $handler->display->display_options['filters']['published']['id'] = 'published';
  $handler->display->display_options['filters']['published']['table'] = 'election_candidate';
  $handler->display->display_options['filters']['published']['field'] = 'published';
  $handler->display->display_options['filters']['published']['value'] = array(
    1 => '1',
  );
  /* Filter criterion: Election candidate: Candidate status */
  $handler->display->display_options['filters']['cstatus']['id'] = 'cstatus';
  $handler->display->display_options['filters']['cstatus']['table'] = 'election_candidate';
  $handler->display->display_options['filters']['cstatus']['field'] = 'cstatus';
  $handler->display->display_options['filters']['cstatus']['operator'] = 'not in';
  $handler->display->display_options['filters']['cstatus']['value'] = array(
    -2 => '-2',
  );

  /* Display: Nominees listing */
  $handler = $view->new_display('page', 'Nominees listing', 'page_1');
  $handler->display->display_options['path'] = 'election/%/candidates';

  /* Display: About this nominee */
  $handler = $view->new_display('block', 'About this nominee', 'block_1');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['row_class'] = 'elec-about-block';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'There are currently no published candidate profiles for this election';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'There are currently no published candidate profiles for this election.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Election candidate: User */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'election_candidate';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Election: Election ID */
  $handler->display->display_options['fields']['election_id']['id'] = 'election_id';
  $handler->display->display_options['fields']['election_id']['table'] = 'election';
  $handler->display->display_options['fields']['election_id']['field'] = 'election_id';
  $handler->display->display_options['fields']['election_id']['label'] = '';
  $handler->display->display_options['fields']['election_id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['election_id']['element_label_colon'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = 'Back to nominations list';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['path'] = 'election/[election_id]/candidates';
  $handler->display->display_options['fields']['nothing']['alter']['link_class'] = 'action-button';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Field: Election candidate: Picture */
  $handler->display->display_options['fields']['field_election_picture']['id'] = 'field_election_picture';
  $handler->display->display_options['fields']['field_election_picture']['table'] = 'field_data_field_election_picture';
  $handler->display->display_options['fields']['field_election_picture']['field'] = 'field_election_picture';
  $handler->display->display_options['fields']['field_election_picture']['label'] = '';
  $handler->display->display_options['fields']['field_election_picture']['element_class'] = 'candidatepicture-preview';
  $handler->display->display_options['fields']['field_election_picture']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_election_picture']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_election_picture']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'uid';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name']['link_to_user'] = FALSE;
  /* Field: User: Drupal.org uid */
  $handler->display->display_options['fields']['drupalorg_uid']['id'] = 'drupalorg_uid';
  $handler->display->display_options['fields']['drupalorg_uid']['table'] = 'views_entity_user';
  $handler->display->display_options['fields']['drupalorg_uid']['field'] = 'drupalorg_uid';
  $handler->display->display_options['fields']['drupalorg_uid']['relationship'] = 'uid';
  $handler->display->display_options['fields']['drupalorg_uid']['label'] = '';
  $handler->display->display_options['fields']['drupalorg_uid']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['drupalorg_uid']['alter']['text'] = '[name]';
  $handler->display->display_options['fields']['drupalorg_uid']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['drupalorg_uid']['alter']['path'] = 'https://www.drupal.org/user/[drupalorg_uid]';
  $handler->display->display_options['fields']['drupalorg_uid']['element_class'] = 'druprofilelink';
  $handler->display->display_options['fields']['drupalorg_uid']['element_label_colon'] = FALSE;
  /* Field: Field: Crosssite account created */
  $handler->display->display_options['fields']['field_crosssite_account_created']['id'] = 'field_crosssite_account_created';
  $handler->display->display_options['fields']['field_crosssite_account_created']['table'] = 'field_data_field_crosssite_account_created';
  $handler->display->display_options['fields']['field_crosssite_account_created']['field'] = 'field_crosssite_account_created';
  $handler->display->display_options['fields']['field_crosssite_account_created']['relationship'] = 'uid';
  $handler->display->display_options['fields']['field_crosssite_account_created']['label'] = 'Drupal.org account age';
  $handler->display->display_options['fields']['field_crosssite_account_created']['type'] = 'format_interval';
  $handler->display->display_options['fields']['field_crosssite_account_created']['settings'] = array(
    'interval' => '2',
    'interval_display' => 'raw time ago',
  );
  /* Field: Election candidate: Name */
  $handler->display->display_options['fields']['candidate_name']['id'] = 'candidate_name';
  $handler->display->display_options['fields']['candidate_name']['table'] = 'field_data_candidate_name';
  $handler->display->display_options['fields']['candidate_name']['field'] = 'candidate_name';
  $handler->display->display_options['fields']['candidate_name']['label'] = '';
  $handler->display->display_options['fields']['candidate_name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['candidate_name']['element_type'] = 'h3';
  $handler->display->display_options['fields']['candidate_name']['element_class'] = 'candidatename-preview';
  $handler->display->display_options['fields']['candidate_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['candidate_name']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['candidate_name']['click_sort_column'] = 'title';
  $handler->display->display_options['fields']['candidate_name']['settings'] = array(
    'format' => 'default',
    'markup' => 0,
    'output' => 'default',
    'multiple' => 'default',
    'multiple_delimiter' => ', ',
    'multiple_and' => 'text',
    'multiple_delimiter_precedes_last' => 'never',
    'multiple_el_al_min' => '3',
    'multiple_el_al_first' => '1',
  );
  /* Field: Election candidate: Twitter handle */
  $handler->display->display_options['fields']['field_twitter']['id'] = 'field_twitter';
  $handler->display->display_options['fields']['field_twitter']['table'] = 'field_data_field_twitter';
  $handler->display->display_options['fields']['field_twitter']['field'] = 'field_twitter';
  $handler->display->display_options['fields']['field_twitter']['element_class'] = 'candidatetwit-preview';
  $handler->display->display_options['fields']['field_twitter']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_twitter']['type'] = 'twitterfield_twitter_link';
  /* Field: Field: About Me */
  $handler->display->display_options['fields']['field_about_me']['id'] = 'field_about_me';
  $handler->display->display_options['fields']['field_about_me']['table'] = 'field_data_field_about_me';
  $handler->display->display_options['fields']['field_about_me']['field'] = 'field_about_me';
  $handler->display->display_options['fields']['field_about_me']['label'] = '';
  $handler->display->display_options['fields']['field_about_me']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_about_me']['alter']['max_length'] = '120';
  $handler->display->display_options['fields']['field_about_me']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['field_about_me']['alter']['more_link_text'] = '(more)';
  $handler->display->display_options['fields']['field_about_me']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['field_about_me']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Election candidate: Candidate ID */
  $handler->display->display_options['arguments']['candidate_id']['id'] = 'candidate_id';
  $handler->display->display_options['arguments']['candidate_id']['table'] = 'election_candidate';
  $handler->display->display_options['arguments']['candidate_id']['field'] = 'candidate_id';
  $handler->display->display_options['arguments']['candidate_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['candidate_id']['default_argument_type'] = 'raw';
  $handler->display->display_options['arguments']['candidate_id']['default_argument_options']['index'] = '1';
  $handler->display->display_options['arguments']['candidate_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['candidate_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['candidate_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['candidate_id']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['candidate_id']['validate']['type'] = 'numeric';
  $export['nominations'] = $view;

  return $export;
}
