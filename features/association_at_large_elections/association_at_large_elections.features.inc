<?php

/**
 * @file
 * association_at_large_elections.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function association_at_large_elections_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function association_at_large_elections_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function association_at_large_elections_image_default_styles() {
  $styles = array();

  // Exported image style: candidate_thumbnail.
  $styles['candidate_thumbnail'] = array(
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 180,
          'height' => 180,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'candidate_thumbnail',
  );

  return $styles;
}

/**
 * Implements hook_default_reply_bundle().
 */
function association_at_large_elections_default_reply_bundle() {
  $items = array();
  $items['candidate_comments'] = entity_import('reply_bundle', '{
    "bundle" : "candidate_comments",
    "name" : "Comment to candidate",
    "access" : "2",
    "display" : "2",
    "description" : "",
    "form" : "1",
    "allow_reply" : "1",
    "locked" : "0"
  }');
  return $items;
}
