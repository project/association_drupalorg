<?php

/**
 * @file
 * association_users_admin.features.inc
 */

/**
 * Implements hook_views_api().
 */
function association_users_admin_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
