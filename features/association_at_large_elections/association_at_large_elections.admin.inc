<?php
/**
 * @file
 * Association.drupal.org settings management.
 */

/**
 *
 */
function admin_election_association() {
  $form = array();

  $form['association_at_large_elections_block_nomination'] = array(
    '#title' => t('Election post id'),
    '#description' => t('Enter the id of the current election post'),
    '#type' => 'textfield',
    '#size' => 3,
    '#maxlength' => 5,
    '#default_value' => variable_get('association_at_large_elections_block_nomination', NULL),
  );

  $form['association_at_large_elections_text_pre_vote'] = array(
    '#title' => t('Text to display in the voting page'),
    '#description' => t('This text will be displayed on top of the voting page where the user selects the ranking of the users.'),
    '#type' => 'text_format',
    // Input format hardcoded as we're in an admin screen anyways and wouldn't
    // make a lot of sense to use another variable to store this.
    '#format' => 'full_html',
    '#default_value' => variable_get('association_at_large_elections_text_pre_vote', NULL),
  );

  $form['association_at_large_elections_text_post_vote'] = array(
    '#title' => t('Text to display in the the preview voting page'),
    '#description' => t('This text will be displayed on top of the preview of voting page where the user confirm their vote selection.'),
    '#type' => 'text_format',
    '#format' => 'full_html',
    '#default_value' => variable_get('association_at_large_elections_text_post_vote', NULL),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * We need to set manually these variables as system_settings_form() is not
 * smart enough to deal with format and value.
 * See https://drupal.org/node/820816
 */
function admin_election_association_submit($form, &$form_state) {
  variable_set('association_at_large_elections_block_nomination', $form_state['values']['association_at_large_elections_block_nomination']);
  variable_set('association_at_large_elections_text_pre_vote', check_markup($form_state['values']['association_at_large_elections_text_pre_vote']['value'], $form_state['values']['association_at_large_elections_text_pre_vote']['format']));
  variable_set('association_at_large_elections_text_post_vote', check_markup($form_state['values']['association_at_large_elections_text_post_vote']['value'], $form_state['values']['association_at_large_elections_text_post_vote']['format']));
}
