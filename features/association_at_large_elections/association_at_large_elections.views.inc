<?php
///**
// * Implementation of hook_views_plugins().
// */
//function association_at_large_elections_views_plugins() {
//  $plugin = array();
//  $plugin['query']['association_at_large_elections_plugin_query'] = array(
//    'title' => t('association at large elections query'),
//    'help' => t('association at large elections query object.'),
//    'handler' => 'association_at_large_elections_plugin_query',
//  );
//  return $plugin;
//}

/**
 * Implementation of hook_views_data().
 */
function association_at_large_elections_views_data() {
  $data = array();

  // Base data
  $data['association_at_large_elections']['table']['group'] = t('Association at Large Elections');
  $data['association_at_large_elections']['table']['base'] = array(
    'title' => t('Association at Large Elections'),
    'help' => t('Association at Large Elections Data'),
    'query class' => 'association_at_large_elections_plugin_query'
  );

  // Fields
  $data['association_at_large_elections']['drupalorguserid'] = array(
    'title' => t('Drupal.org UserID'),
    'help' => t('The drupalorg ID of this user.'),
    'field' => array(
      'handler' => 'association_at_large_elections_handler_field',
    ),
  );
}