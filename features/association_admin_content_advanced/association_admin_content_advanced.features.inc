<?php
/**
 * @file
 * association_admin_content_advanced.features.inc
 */

/**
 * Implements hook_views_api().
 */
function association_admin_content_advanced_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
